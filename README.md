# Knapsack Solver

## How to run
This code was built and tested on Linux Ubuntu 16.10 using 
mono 4.7

To run, assuming workind directory is `KnapsackSolver/bin/Debug`
```
mono KnapsackSolver.exe ../../../bitflyer.txt 1000000
```
This gives the total reward: *14.2593* 
and total cost 989152 bytes.

For testing,
```
mono KnapsackSolver.exe ../../../bitflyer.txt 500000
```
gives the total reward: *13.4376*
and total cost 499032 bytes.


## Method used
The method I used for solving this problem is to use a Branch-and-Bound
Knapsack Problem solver. The solution is generic and can be used for much
larger  datasets, and can give estimate results for large problems.

## Author
Jonathan Hough 29/1/2018
