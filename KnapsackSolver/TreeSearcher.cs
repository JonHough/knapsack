﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KnapsackSolver
{
	/// <summary>
	/// Tree searcher. Creates a search tree to find the 
	/// optimal solution, using a Branch-and-Bound method. 
	/// The optimal
	/// </summary>
	public class TreeSearcher
	{
		private float m_remainingCapacity;
		private float m_totalCapacity;
		private float m_currentValue;
		private List<Item> m_items;
		private int m_index;
		private List<int> m_notUsedIndices;
		private float m_degradeRate;
		private List<int> m_prevList;
		private float m_optimisticValue = 0;

		public float CurrentValue{ get { return m_currentValue; } }

		public float OptimisticValue{ get { return m_optimisticValue; } }

		public float RemainingCapacity{ get { return m_remainingCapacity; } }

		public float DegradeRate{ get { return m_degradeRate; } }

		public List<int> PrevList{ get { return m_prevList; } }

		public TreeSearcher (float remainingCapacity, float totalCapacity, float currentValue,
			List<Item> items, int index, List<int> notUseIndices, float degradeRate, List<int> prevlist, bool useOptimisticEstimate)
		{
			this.m_remainingCapacity = remainingCapacity;
			this.m_totalCapacity = totalCapacity;
			this.m_currentValue = currentValue;
			this.m_items = items;
			this.m_index = index;
			this.m_notUsedIndices = notUseIndices;
			this.m_optimisticValue = useOptimisticEstimate ? EstimateMax (remainingCapacity) : AddHeuristic (false);
			this.m_degradeRate = degradeRate;
			this.m_prevList = prevlist;


		}

		public float AddHeuristic (bool useMaxEstimate)
		{
			if (useMaxEstimate) {
				return m_currentValue +  EstimateMax (m_remainingCapacity);
			} else {
				return m_items.Sum (x => x.value);
			}
		}

		public float EstimateMax (float capacity)
		{
			float f = 0;
			float c = capacity;
			List<Item> sortedItems = (from it in m_items orderby it.value descending
			                          select it).ToList ();
			for (int i = 0; i < sortedItems.Count; i++) {
				var item = sortedItems [i];
				if (f + item.frac >= c) {
					f += item.value * c / item.weight;
					break;
				} else {
					f += item.value;
					c -= item.weight;
				}
			}
			return f;
		}


		public TreeSearcher Estimate (bool use)
		{
			if (m_index > m_items.Count - 1) {
				return null;
			} else {
				if (use) {
					var rc = m_remainingCapacity - m_items [m_index].weight;
					var cv = m_currentValue + m_items [m_index].value;
					var m = new List<int> (m_prevList);
					m.Add (1);
					var left = new TreeSearcher (rc, m_totalCapacity, cv, m_items, m_index + 1, 
						           new List<int> (m_notUsedIndices), m_degradeRate, m, false);
					return left;
				} else {
					var rc = m_remainingCapacity;
					if (rc < 0)
						return null;
					var cv = m_currentValue;// + m_items [m_index].value;
					var notUsed = new List<int> (m_notUsedIndices);
					notUsed.Add (m_index);
					var m = new List<int> (m_prevList);
					m.Add (0);
					var right = new TreeSearcher (rc, m_totalCapacity, cv, m_items, m_index + 1, 
						            notUsed, m_degradeRate, m, false);
					return right;
				}
			}
		}
	}
}

