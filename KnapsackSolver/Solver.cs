﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KnapsackSolver
{
	public class Solver
	{
		private List<Item> m_items;

		private List<Item> m_sortedItems;
		private float m_capacity;

		public Solver (List<Item> items, float capacity)
		{
			m_items = items;
			m_capacity = capacity;
		}


		public float EstimateMax ()
		{
			float f = 0;
			float c = m_capacity;
			List<Item> sortedItems = (from it in m_items orderby it.value descending
			                          select it).ToList ();
			for (int i = 0; i < sortedItems.Count; i++) {
				 
				var item = sortedItems [i];
				if (f + item.frac > c) {
					f += item.value * c / item.weight;
					break;
				} else {
					f += item.value;
					c -= m_capacity;
				}
			}
			return f;
		}


		public void Search ()
		{
			m_sortedItems = (from it in m_items orderby it.frac descending
			                 select it).ToList ();
			Stack<TreeSearcher> stack = new Stack<TreeSearcher> ();
		 
			var searcher = new TreeSearcher (m_capacity, m_capacity, 0, m_sortedItems, 0, new List<int> (), 1.0f, new List<int> (), false);
			stack.Push (searcher);
			float bestFoundSolution = 0; 
			TreeSearcher foundSolution = null;

			while (stack.Count > 0) {
				var next = stack.Pop ();
				if (next == null)
					continue;
				if (next.CurrentValue >= bestFoundSolution) {
					foundSolution = next;
					bestFoundSolution = next.CurrentValue;
				}

				var sLeft = next.Estimate (true);
				if (sLeft == null)
					continue;
				if (sLeft.OptimisticValue < bestFoundSolution || sLeft.RemainingCapacity < 0) {
					// ignore
				} else {
					stack.Push (sLeft);
				}

				var sRight = next.Estimate (false);
				if (sRight != null) {
					if (sRight.OptimisticValue * sRight.DegradeRate < bestFoundSolution || sRight.RemainingCapacity < 0) {
						// ignore
					} else {
						stack.Push (sRight);
					}
				}
 
			}

			int ctr = 0;
			var usedIndices = new List<int> ();
			float total = 0;
			float totalValue = 12.5f; 
			foreach (var s in foundSolution.PrevList) {
				if (s == 1) {
					usedIndices.Add (ctr);
					totalValue += m_sortedItems [ctr].value;
					total += m_sortedItems [ctr].weight;
				}
				ctr++;
			}
			Console.WriteLine ("Total cost: " + total);
			Console.WriteLine ("Total reward: " + totalValue);
		}
	}
}

