﻿using System;

namespace KnapsackSolver
{
	public struct Item
	{
		public float value;
		public float weight;
		public float frac;
		
		public Item (float value, float weight)
		{
			if (value < 0)
				throw new Exception ("Item value must be non-negative.");
			if (weight < 0)
				throw new Exception ("Item weight must be non-negative");
			this.value = value;
			this.weight = weight;
			if (weight != 0) {
				this.frac = value / weight;
			} else
				this.frac = float.PositiveInfinity;
		}
	}
}

