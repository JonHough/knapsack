﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KnapsackSolver
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			if (args.Length != 2)
				throw new Exception ("Two arguments are required - 1. the path to the file, 2. the maximum capacity in bytes.");

			int maxCapacity = int.Parse (args [1]);
			List<float> values = new List<float> ();
			List<float> weights = new List<float> ();
			foreach (string line in File.ReadLines(args[0])) {
				string[] splitLine = line.Split ('\t');

				if (!splitLine [0].StartsWith ("#") && splitLine.Length >= 3) {
					float weight = float.Parse (splitLine [1]);
					float val = float.Parse (splitLine [2]); 
					values.Add (val);
					weights.Add (weight);
				}
			}

			float[] w = new float[] {
				57247,
				98732,
				134928,
				77275,
				29240,
				15440,
				70820,
				139603,
				63718,
				143807,
				190457,
				40572  
			};

			float[] v = new float[] {
				0.0887f,
				0.1856f,
				0.2307f,
				0.1522f,
				0.0532f,
				0.0250f,
				0.1409f,
				0.2541f,
				0.1147f,
				0.2660f,
				0.2933f,
				0.0686f
			};

			List<Item> l = new List<Item> ();
			for (int i = 0; i < v.Length; i++) {
				l.Add (new Item (values [i], weights [i]));
			}

			Solver s = new Solver (l, maxCapacity);
			s.Search ();
		}
	}
}
